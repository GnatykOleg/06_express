module.exports = {
    // The length of the string that prettier will wrap.
    printWidth: 80,

    // Number of spaces per indent level.
    tabWidth: 4,

    // Semicolons at the end of statements.
    semi: true,

    // Single quotes instead of double quotes singleQuote: true,
    singleQuote: true,

    // Trailing commas wherever possible. As an example arrays, objects.
    trailingComma: 'all',

    // Leave parentheses at the only parameter of the arrow function, for typing.
    arrowParens: 'always',
};
