import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

import { Lector } from '../entities/lector.entity';

// CONTROLLERS INTERFACES

export interface ILectorCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<Lector, 'id'>;
}

export interface ILectorAddCourseRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: { courseId: string };
    [ContainerTypes.Params]: { id: string };
}

// SERVICE INTERFACES

interface ILectorId {
    lectorId: string;
}

export interface ILectorAddCourseServiceParams extends ILectorId {
    courseId: string;
}
