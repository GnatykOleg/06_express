import { Router } from 'express';

import controllerWrapper from '../application/utilities/controller-wrapper.utility';

import validator from '../application/middlewares/validation.middleware';

import { idParamSchema } from '../application/schemas/id-param.schema';

import { lectorAddCourSchema, lectorCreateSchema } from './lector.schema';

import * as lectorController from './lector.contoller';

const router = Router();

router.get('/', controllerWrapper(lectorController.getAllLectors));

router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(lectorController.getLectorInfoWithCoursesById),
);

router.post(
    '/',
    validator.body(lectorCreateSchema),
    controllerWrapper(lectorController.createLector),
);

router.post(
    '/:id/course',
    validator.params(idParamSchema),
    validator.body(lectorAddCourSchema),
    controllerWrapper(lectorController.addToLectorCourseById),
);

export default router;
