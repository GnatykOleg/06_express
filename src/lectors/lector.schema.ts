import Joi from 'joi';

import { Lector } from './entities/lector.entity';

export const lectorCreateSchema = Joi.object<Omit<Lector, 'id'>>({
    name: Joi.string().trim().lowercase().required(),
    email: Joi.string().email().trim().lowercase().required(),
    password: Joi.string().min(4).alphanum().required(),
});

export const lectorAddCourSchema = Joi.object<{ courseId: string }>({
    courseId: Joi.string().lowercase().guid().required(),
});
