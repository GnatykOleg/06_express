import { Request, Response } from 'express';

import { HttpStatusesEnum } from '../application/enums/application.enum';

import { ValidatedRequest } from 'express-joi-validation';

import {
    ILectorAddCourseRequest,
    ILectorCreateRequest,
} from './interfaces/lector.interface';

import * as lectorService from './lector.service';

export const getAllLectors = async (
    _: Request,
    res: Response,
): Promise<void> => {
    const allLectors = await lectorService.getAllLectors();

    res.status(HttpStatusesEnum.OK).json(allLectors);
};

export const getLectorInfoWithCoursesById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    const lectorInfoWithCourses =
        await lectorService.getLectorInfoWithCoursesById(req.params.id);

    res.status(HttpStatusesEnum.OK).json(lectorInfoWithCourses);
};

export const createLector = async (
    req: ValidatedRequest<ILectorCreateRequest>,
    res: Response,
): Promise<void> => {
    const createdLector = await lectorService.createLector(req.body);

    res.status(HttpStatusesEnum.CREATED).json(createdLector);
};

export const addToLectorCourseById = async (
    req: ValidatedRequest<ILectorAddCourseRequest>,
    res: Response,
): Promise<void> => {
    await lectorService.addToLectorCourseById({
        lectorId: req.params.id,
        courseId: req.body.courseId,
    });

    res.status(HttpStatusesEnum.NO_CONTENT).json();
};
