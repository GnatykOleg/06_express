import bcrypt from 'bcrypt';

import AppDataSource from '../configurations/database/data-source.configuration';

import HttpException from '../application/exceptions/http.exception';

import { Lector } from './entities/lector.entity';

import { Course } from '../courses/entities/course.entity';

import { ILectorAddCourseServiceParams } from './interfaces/lector.interface';

import {
    HttpErrorMessagesEnum,
    HttpStatusesEnum,
} from '../application/enums/application.enum';

import { LectorCourse } from './entities/lector-course.entity';

const lectorsRepository = AppDataSource.getRepository(Lector);
const courseRepository = AppDataSource.getRepository(Course);
const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

export const getAllLectors = async (): Promise<Lector[] | []> => {
    const allLectors: Promise<Lector[] | []> = lectorsRepository.find({});

    return allLectors;
};

export const getLectorInfoWithCoursesById = async (
    lectorId: string,
): Promise<Lector> => {
    const lectorInfoWithCourses: Lector | null = await lectorsRepository
        .createQueryBuilder('lector')
        .leftJoinAndSelect('lector.courses', 'courses')
        .where('lector.id = :lectorId', { lectorId })
        .getOne();

    if (!lectorInfoWithCourses)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.LECTOR_NOT_FOUND,
        );

    return lectorInfoWithCourses;
};

export const createLector = async (
    newLectorData: Omit<Lector, 'id'>,
): Promise<Lector> => {
    const { email, password } = newLectorData;

    const foundLector = await lectorsRepository.findOneBy({ email });

    if (foundLector)
        throw new HttpException(
            HttpStatusesEnum.CONFLICT,
            HttpErrorMessagesEnum.LECTOR_ALREDY_EXIST,
        );

    const hashPassword = await bcrypt.hash(password, 10);

    const newLector = lectorsRepository.save({
        ...newLectorData,
        password: hashPassword,
    }) as Promise<Lector>;

    return newLector;
};

export const addToLectorCourseById = async ({
    lectorId,
    courseId,
}: ILectorAddCourseServiceParams): Promise<void> => {
    const lectorToUpdate = await lectorsRepository.findOneBy({ id: lectorId });

    if (!lectorToUpdate)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.LECTOR_NOT_FOUND,
        );

    const foundCourse = await courseRepository.findOneBy({ id: courseId });

    if (!foundCourse)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.COURSE_NOT_FOUND,
        );

    await lectorCourseRepository.save({ courseId, lectorId });
};
