import AppDataSource from '../configurations/database/data-source.configuration';

import HttpException from '../application/exceptions/http.exception';

import { Course } from './entities/course.entity';

import {
    HttpErrorMessagesEnum,
    HttpStatusesEnum,
} from '../application/enums/application.enum';

import { ICoursesForLector } from './interfaces/course.interfaces';

const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[] | []> => {
    const allCourses: Promise<Course[] | []> = coursesRepository.find({});

    return allCourses;
};

export const createCourse = async (
    newCourseData: Omit<Course, 'id'>,
): Promise<Course> => {
    const { name } = newCourseData;

    const foundCourse = await coursesRepository.findOneBy({ name });

    if (foundCourse)
        throw new HttpException(
            HttpStatusesEnum.CONFLICT,
            HttpErrorMessagesEnum.COURSE_ALREDY_EXIST,
        );

    const newCourse = coursesRepository.save(newCourseData) as Promise<Course>;

    return newCourse;
};

export const getCoursesForLectorById = async (
    lectorId: string,
): Promise<ICoursesForLector[]> => {
    const lectorInfoWithCourses: ICoursesForLector[] | [] =
        await coursesRepository
            .createQueryBuilder('course')
            .select([
                'lector.name as "lectorName"',
                'course.name as "courseName", course.id as "courseId"',
            ])
            .leftJoin('course.lectors', 'lector')
            .where('lector.id = :lectorId', { lectorId })
            .getRawMany();

    if (lectorInfoWithCourses.length === 0)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.COURSE_NOT_FOUND,
        );

    return lectorInfoWithCourses;
};
