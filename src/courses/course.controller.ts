import { Request, Response } from 'express';

import { HttpStatusesEnum } from '../application/enums/application.enum';

import * as courseService from './course.service';

import { ValidatedRequest } from 'express-joi-validation';

import { ICourseCreateRequest } from './interfaces/course.interfaces';

export const getAllCourses = async (
    _: Request,
    res: Response,
): Promise<void> => {
    const allCourses = await courseService.getAllCourses();

    res.status(HttpStatusesEnum.OK).json(allCourses);
};

export const createCourse = async (
    req: ValidatedRequest<ICourseCreateRequest>,
    res: Response,
): Promise<void> => {
    const createdCourse = await courseService.createCourse(req.body);

    res.status(HttpStatusesEnum.CREATED).json(createdCourse);
};

export const getCoursesForLectorById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    const lectorCourses = await courseService.getCoursesForLectorById(
        req.params.id,
    );

    res.status(HttpStatusesEnum.OK).json(lectorCourses);
};
