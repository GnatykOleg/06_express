import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

import { Course } from '../entities/course.entity';

// CONTROLLERS INTERFACES
export interface ICourseCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<Course, 'id'>;
}

// SERVICE INTERFACES

export interface ICoursesForLector {
    lectorName: string;
    courseName: string;
    courseId: string;
}
