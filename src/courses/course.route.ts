import { Router } from 'express';

import controllerWrapper from '../application/utilities/controller-wrapper.utility';

import validator from '../application/middlewares/validation.middleware';

import * as courseController from './course.controller';

import { courseCreateSchema } from './course.schema';

import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

router.get('/', controllerWrapper(courseController.getAllCourses));

router.get(
    '/:id/lector',
    validator.params(idParamSchema),
    controllerWrapper(courseController.getCoursesForLectorById),
);

router.post(
    '/',
    validator.body(courseCreateSchema),
    controllerWrapper(courseController.createCourse),
);

export default router;
