import Joi from 'joi';
import { Course } from './entities/course.entity';

export const courseCreateSchema = Joi.object<Omit<Course, 'id'>>({
    name: Joi.string().trim().lowercase().required(),
    description: Joi.string().trim().lowercase().required(),
    hours: Joi.number().min(1).max(1000).required(),
});

export const courseUpdateSchema = Joi.object<Partial<Course>>({
    name: Joi.string().trim().lowercase().optional(),
    description: Joi.string().trim().lowercase().optional(),
    hours: Joi.number().min(1).max(1000).optional(),
});
