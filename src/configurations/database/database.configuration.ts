import { DataSourceOptions } from 'typeorm';

const {
    DATABASE_PORT,
    DATABASE_HOST,
    DATABASE_USERNAME,
    DATABASE_PASSWORD,
    DATABASE_NAME,
} = process.env;

const databaseConfiguration = (isMigrationRun = true): DataSourceOptions => {
    const ROOT_PATH: string = process.cwd();
    const entitiesPath = `${ROOT_PATH}/**/*.entity{.ts,.js}`;
    const migrationPath = `${ROOT_PATH}/**/migrations/*{.ts,.js}`;

    return {
        type: 'postgres',
        host: DATABASE_HOST,
        port: Number(DATABASE_PORT),
        username: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
        database: DATABASE_NAME,

        entities: [entitiesPath],
        migrations: [migrationPath],
        migrationsTableName: 'migrations',
        migrationsRun: isMigrationRun,

        synchronize: false,
        logging: true,
    };
};

export default databaseConfiguration;
