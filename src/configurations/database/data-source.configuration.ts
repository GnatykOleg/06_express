import { DataSource } from 'typeorm';

import databaseConfiguration from './database.configuration';

const AppDataSource = new DataSource(databaseConfiguration());

export default AppDataSource;
