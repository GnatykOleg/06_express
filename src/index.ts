import 'dotenv/config';

import app from './application/app';

const { SERVER_PORT = 4000 } = process.env;

const startServer = async (): Promise<void> => {
    const startServerMessage = `Server started at port: ${SERVER_PORT}`;

    app.listen(SERVER_PORT, () => console.log(startServerMessage));
};

startServer();
