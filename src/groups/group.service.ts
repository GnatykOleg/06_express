import { DeleteResult } from 'typeorm';

import HttpException from '../application/exceptions/http.exception';

import AppDataSource from '../configurations/database/data-source.configuration';

import { Group } from './entities/group.entity';

import { IGroupUpdateServiceParams } from './interfaces/group.interface';

import {
    HttpErrorMessagesEnum,
    HttpStatusesEnum,
} from '../application/enums/application.enum';

export const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[] | []> => {
    const allGroups = groupsRepository
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.students', 'student')
        .getMany();

    return allGroups;
};

export const getGroupById = async (groupId: string): Promise<Group> => {
    const foundGroup = await groupsRepository
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.students', 'student')
        .where(`group.id = :groupId`, { groupId })
        .getOne();

    if (!foundGroup)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.GROUP_NOT_FOUND,
        );

    return foundGroup;
};

export const createGroup = async (
    newGroupData: Omit<Group, 'id'>,
): Promise<Group> => {
    const foundGroup = await groupsRepository.findOneBy({
        name: newGroupData.name,
    });

    if (foundGroup)
        throw new HttpException(
            HttpStatusesEnum.CONFLICT,
            HttpErrorMessagesEnum.GROUP_ALREDY_EXIST,
        );

    const newGroup = groupsRepository.save(newGroupData) as Promise<Group>;

    return newGroup;
};

export const updateGroupById = async ({
    groupId,
    groupData,
}: IGroupUpdateServiceParams): Promise<Group> => {
    const updateGroup = await groupsRepository.update(groupId, groupData);

    if (!updateGroup.affected)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.GROUP_NOT_FOUND,
        );

    const result = groupsRepository.findOneBy({
        id: groupId,
    }) as Promise<Group>;

    return result;
};

export const deleteGroupById = async (
    groupId: string,
): Promise<DeleteResult> => {
    const result = await groupsRepository.delete(groupId);

    if (!result.affected)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.GROUP_NOT_FOUND,
        );

    return result;
};
