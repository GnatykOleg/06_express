import controllerWrapper from '../application/utilities/controller-wrapper.utility';

import validator from '../application/middlewares/validation.middleware';

import { idParamSchema } from '../application/schemas/id-param.schema';

import { Router } from 'express';

import { groupCreateSchema, groupUpdateSchema } from './group.schema';

import * as groupController from './group.controller';

const router = Router();

router.get('/', controllerWrapper(groupController.getAllGroups));

router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.getGroupById),
);

router.post(
    '/',
    validator.body(groupCreateSchema),
    controllerWrapper(groupController.createGroup),
);

router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(groupUpdateSchema),
    controllerWrapper(groupController.updateGroupById),
);

router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.deleteGroupById),
);

export default router;
