import Joi from 'joi';

import { Group } from './entities/group.entity';

export const groupCreateSchema = Joi.object<Omit<Group, 'id'>>({
    name: Joi.string().trim().lowercase().required(),
});

export const groupUpdateSchema = Joi.object<Partial<Group>>({
    name: Joi.string().trim().lowercase().optional(),
});
