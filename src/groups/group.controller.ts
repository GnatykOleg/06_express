import { Request, Response } from 'express';

import { HttpStatusesEnum } from '../application/enums/application.enum';

import * as groupSevice from './group.service';

import { ValidatedRequest } from 'express-joi-validation';

import {
    IGroupCreateRequest,
    IGroupUpdateRequest,
} from './interfaces/group.interface';

export const getAllGroups = async (
    _: Request,
    res: Response,
): Promise<void> => {
    const allGroups = await groupSevice.getAllGroups();

    res.status(HttpStatusesEnum.OK).json(allGroups);
};

export const getGroupById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    const foundGroup = await groupSevice.getGroupById(req.params.id);

    res.status(HttpStatusesEnum.OK).json(foundGroup);
};

export const createGroup = async (
    req: ValidatedRequest<IGroupCreateRequest>,
    res: Response,
): Promise<void> => {
    const createdGroup = await groupSevice.createGroup(req.body);
    res.status(HttpStatusesEnum.CREATED).json(createdGroup);
};

export const updateGroupById = async (
    req: ValidatedRequest<IGroupUpdateRequest>,
    res: Response,
): Promise<void> => {
    const updatedGroup = await groupSevice.updateGroupById({
        groupId: req.params.id,
        groupData: req.body,
    });

    res.status(HttpStatusesEnum.OK).json(updatedGroup);
};

export const deleteGroupById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    await groupSevice.deleteGroupById(req.params.id);

    res.status(HttpStatusesEnum.NO_CONTENT).send();
};
