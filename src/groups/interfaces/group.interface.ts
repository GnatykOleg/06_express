import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

import { Group } from '../entities/group.entity';

export interface IGroupCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<Group, 'id'>;
}

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<Group>;
    [ContainerTypes.Params]: { id: string };
}

export interface IGroupUpdateServiceParams {
    groupId: string;
    groupData: Partial<Group>;
}
