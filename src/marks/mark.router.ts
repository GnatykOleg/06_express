import { Router } from 'express';

import controllerWrapper from '../application/utilities/controller-wrapper.utility';

import validator from '../application/middlewares/validation.middleware';

import * as markController from './mark.controller';

import { markAddToStudentSchema } from './mark.schema';

import { generateQuerySchema } from '../application/schemas/id-param.schema';

const router = Router();

router.get(
    '/student',
    validator.query(generateQuerySchema('studentId')),
    controllerWrapper(markController.getAllStudentsMarksByStudentId),
);

router.get(
    '/course',
    validator.query(generateQuerySchema('courseId')),
    controllerWrapper(markController.getAllCourseMarksByCourseId),
);

router.post(
    '/',
    validator.body(markAddToStudentSchema),
    controllerWrapper(markController.addMarkToStudent),
);

export default router;
