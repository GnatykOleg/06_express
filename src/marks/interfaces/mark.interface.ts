import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

export interface INewMark {
    mark: number;
    courseId: string;
    studentId: string;
    lectorId: string;
}

export interface IAddNewMarkRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: INewMark;
}

export interface IGetAllStudentMarks extends ValidatedRequestSchema {
    [ContainerTypes.Query]: { studentId: string };
}

export interface IGetAllCourseMarks extends ValidatedRequestSchema {
    [ContainerTypes.Query]: { courseId: string };
}

export interface ICoursesMarks {
    mark: number;
    courseName: string;
    lectorName: string;
    studentName: string;
}

export interface IStudentMarks {
    mark: number;
    courseName: string;
}
