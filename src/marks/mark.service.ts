import {
    HttpErrorMessagesEnum,
    HttpStatusesEnum,
} from '../application/enums/application.enum';

import HttpException from '../application/exceptions/http.exception';

import AppDataSource from '../configurations/database/data-source.configuration';

import { Course } from '../courses/entities/course.entity';

import { Lector } from '../lectors/entities/lector.entity';

import { Student } from '../students/entities/student.entity';

import { Mark } from './entities/mark.entity';

import {
    ICoursesMarks,
    INewMark,
    IStudentMarks,
} from './interfaces/mark.interface';

const marksRepository = AppDataSource.getRepository(Mark);
const studentsRepository = AppDataSource.getRepository(Student);
const lectorsRepository = AppDataSource.getRepository(Lector);
const coursesRepository = AppDataSource.getRepository(Course);

export const getAllStudentsMarksByStudentId = async (
    studentId: string,
): Promise<IStudentMarks[]> => {
    const studentMarks: IStudentMarks[] | [] = await marksRepository
        .createQueryBuilder('mark')
        .select(['mark.mark as "mark"', 'course.name as "courseName"'])
        .leftJoin('mark.course', 'course')
        .where('mark.student_id = :studentId', { studentId })
        .getRawMany();

    if (studentMarks.length === 0)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.MARK_NOT_FOUND,
        );

    return studentMarks;
};

export const getAllCourseMarksByCourseId = async (
    courseId: string,
): Promise<ICoursesMarks[]> => {
    const coursesMarks: ICoursesMarks[] | [] = await marksRepository
        .createQueryBuilder('mark')
        .select([
            'mark.mark as "mark"',
            'course.name as "courseName"',
            'lector.name as "lectorName"',
            'student.name as "studentName"',
        ])
        .leftJoin('mark.course', 'course')
        .leftJoin('mark.lector', 'lector')
        .leftJoin('mark.student', 'student')
        .where('course.id = :courseId', { courseId })
        .getRawMany();

    if (coursesMarks.length === 0)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.MARK_NOT_FOUND,
        );

    return coursesMarks;
};

export const addMarkToStudent = async (
    newMarkData: INewMark,
): Promise<void> => {
    const foundStudent = await studentsRepository.findOneBy({
        id: newMarkData.studentId,
    });

    if (!foundStudent)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    const foundLector = await lectorsRepository.findOneBy({
        id: newMarkData.lectorId,
    });

    if (!foundLector)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.LECTOR_NOT_FOUND,
        );

    const foundCourse = await coursesRepository.findOneBy({
        id: newMarkData.courseId,
    });

    if (!foundCourse)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.COURSE_NOT_FOUND,
        );

    await marksRepository.save(newMarkData);
};
