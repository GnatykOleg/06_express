import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import CoreEntity from '../../application/entities/core.entity';

import { Course } from '../../courses/entities/course.entity';

import { Student } from '../../students/entities/student.entity';

import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
    @Column({ type: 'integer', nullable: false })
    mark: number;

    @ManyToOne(() => Course, (course) => course.marks)
    @JoinColumn({ name: 'course_id' })
    course: Course;

    @Column({ type: 'uuid', name: 'course_id' })
    courseId: string;

    @ManyToOne(() => Student, (student) => student.marks)
    @JoinColumn({ name: 'student_id' })
    student: Student;

    @Column({ type: 'uuid', name: 'student_id' })
    studentId: string;

    @ManyToOne(() => Lector, (lector) => lector.marks)
    @JoinColumn({ name: 'lector_id' })
    lector: Lector;

    @Column({ type: 'uuid', name: 'lector_id' })
    lectorId: string;
}
