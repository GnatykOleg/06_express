import Joi from 'joi';

import { INewMark } from './interfaces/mark.interface';

export const markAddToStudentSchema = Joi.object<INewMark>({
    courseId: Joi.string().lowercase().guid().required(),
    studentId: Joi.string().lowercase().guid().required(),
    lectorId: Joi.string().lowercase().guid().required(),
    mark: Joi.number().min(1).max(10).required(),
});
