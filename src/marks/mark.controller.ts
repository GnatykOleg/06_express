import { Response } from 'express';

import { HttpStatusesEnum } from '../application/enums/application.enum';

import * as markService from './mark.service';

import { ValidatedRequest } from 'express-joi-validation';

import {
    IAddNewMarkRequest,
    IGetAllCourseMarks,
    IGetAllStudentMarks,
} from './interfaces/mark.interface';

export const getAllStudentsMarksByStudentId = async (
    req: ValidatedRequest<IGetAllStudentMarks>,
    res: Response,
): Promise<void> => {
    const { studentId } = req.query;

    const allStudentsMarks = await markService.getAllStudentsMarksByStudentId(
        String(studentId),
    );

    res.status(HttpStatusesEnum.OK).json(allStudentsMarks);
};

export const getAllCourseMarksByCourseId = async (
    req: ValidatedRequest<IGetAllCourseMarks>,
    res: Response,
): Promise<void> => {
    const { courseId } = req.query;

    const allCourseMarks = await markService.getAllCourseMarksByCourseId(
        String(courseId),
    );

    res.status(HttpStatusesEnum.OK).json(allCourseMarks);
};

export const addMarkToStudent = async (
    req: ValidatedRequest<IAddNewMarkRequest>,
    res: Response,
): Promise<void> => {
    await markService.addMarkToStudent(req.body);

    res.status(HttpStatusesEnum.NO_CONTENT).json();
};
