import { Request, Response } from 'express';

import { HttpStatusesEnum } from '../application/enums/application.enum';

import * as studentService from './student.service';

import { ValidatedRequest } from 'express-joi-validation';

import {
    IGetStudentByName,
    IStudentAddGroupRequest,
    IStudentAddImageRequest,
    IStudentCreateRequest,
    IStudentUpdateRequest,
    IStudentWithGroupName,
} from './interfaces/student.interface';

export const getAllStudents = async (
    req: ValidatedRequest<IGetStudentByName>,
    res: Response,
): Promise<void> => {
    const studentName = req.query.name;

    let data: IStudentWithGroupName | IStudentWithGroupName[];

    if (studentName) {
        data = await studentService.getStudentByName(studentName);
    } else {
        data = await studentService.getAllStudents();
    }

    res.status(HttpStatusesEnum.OK).json(data);
};

export const getStudentById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    const foundStudent = await studentService.getStudentById(req.params.id);

    res.status(HttpStatusesEnum.OK).json(foundStudent);
};

export const createStudent = async (
    req: ValidatedRequest<IStudentCreateRequest>,
    res: Response,
): Promise<void> => {
    const createdStudent = await studentService.createStudent(req.body);

    res.status(HttpStatusesEnum.CREATED).json(createdStudent);
};

export const updateStudentById = async (
    req: ValidatedRequest<IStudentUpdateRequest>,
    res: Response,
): Promise<void> => {
    const updatedStudent = await studentService.updateStudentById({
        studentId: req.params.id,
        studentData: req.body,
    });

    res.status(HttpStatusesEnum.OK).json(updatedStudent);
};

export const getStudentImageById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    const studentImage = await studentService.getStudentImageById(
        req.params.id,
    );

    res.status(HttpStatusesEnum.OK).json(studentImage);
};

export const addToStudentImageById = async (
    req: ValidatedRequest<IStudentAddImageRequest>,
    res: Response,
): Promise<void> => {
    const { path } = req.file ?? {};

    const studentWithImage = await studentService.addToStudentImageById({
        studentId: req.params.id,
        filePath: path,
    });

    res.status(HttpStatusesEnum.OK).json(studentWithImage);
};

export const addToStudentGroupById = async (
    req: ValidatedRequest<IStudentAddGroupRequest>,
    res: Response,
): Promise<void> => {
    const studentWithGroup = await studentService.addToStudentGroupById({
        studentId: req.params.id,
        groupId: req.body.groupId,
    });

    res.status(HttpStatusesEnum.OK).json(studentWithGroup);
};

export const deleteStudentById = async (
    req: Request<{ id: string }>,
    res: Response,
): Promise<void> => {
    await studentService.deleteStudentById(req.params.id);

    res.status(HttpStatusesEnum.NO_CONTENT).send();
};
