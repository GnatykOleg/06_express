import Joi from 'joi';

import { Student } from './entities/student.entity';

export const studentCreateSchema = Joi.object<Omit<Student, 'id'>>({
    name: Joi.string().trim().lowercase().required(),
    surname: Joi.string().trim().lowercase().required(),
    email: Joi.string().email().trim().lowercase().required(),
    age: Joi.number().min(18).max(150).required(),
});

export const studentUpdateSchema = Joi.object<Partial<Student>>({
    name: Joi.string().trim().lowercase().optional(),
    surname: Joi.string().trim().lowercase().optional(),
    email: Joi.string().email().trim().lowercase().optional(),
    age: Joi.number().min(18).max(150).optional(),
    groupId: Joi.string().lowercase().guid().optional(),
});

export const studentAddGroupSchema = Joi.object<{ groupId: string }>({
    groupId: Joi.string().lowercase().guid().required(),
});
