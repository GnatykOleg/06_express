import fs from 'fs/promises';

import path from 'path';

import sharp from 'sharp';

import { DeleteResult } from 'typeorm';

import { v4 as uuidv4 } from 'uuid';

import AppDataSource from '../configurations/database/data-source.configuration';

import HttpException from '../application/exceptions/http.exception';

import { Student } from './entities/student.entity';

import {
    IStudentAddGroupServiceParams,
    IStudentAddImageServiceParams,
    IStudentUpdateServiceParams,
    IStudentWithGroupName,
} from './interfaces/student.interface';

import { groupsRepository } from '../groups/group.service';

import {
    HttpErrorMessagesEnum,
    HttpStatusesEnum,
} from '../application/enums/application.enum';

const studentsRepository = AppDataSource.getRepository(Student);

const selectFields = [
    'student.id as id',
    'student.created_at as "createdAt"',
    'student.updated_at as "updatedAt"',
    'student.name as name',
    'student.surname as surname',
    'student.email as email',
    'student.age as age',
    'student.image_path as "imagePath"',
];

export const getAllStudents = async (): Promise<
    IStudentWithGroupName[] | []
> => {
    const allStudents: IStudentWithGroupName[] | [] = await studentsRepository
        .createQueryBuilder('student')
        .select(selectFields)
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .getRawMany();

    return allStudents;
};

export const getStudentById = async (
    studentId: string,
): Promise<IStudentWithGroupName> => {
    const foundStudent: IStudentWithGroupName | undefined =
        await studentsRepository
            .createQueryBuilder('student')
            .select(selectFields)
            .leftJoin('student.group', 'group')
            .addSelect('group.name as "groupName"')
            .where(`student.id = :studentId`, { studentId })
            .getRawOne();

    if (!foundStudent)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    return foundStudent;
};

export const getStudentByName = async (
    studentName: string,
): Promise<IStudentWithGroupName> => {
    const foundStudent: IStudentWithGroupName | undefined =
        await studentsRepository
            .createQueryBuilder('student')
            .select(selectFields)
            .leftJoin('student.group', 'group')
            .addSelect('group.name as "groupName"')
            .where('student.name = :studentName', { studentName })
            .getRawOne();

    if (!foundStudent) {
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );
    }

    return foundStudent;
};

export const createStudent = async (
    newStudentData: Omit<Student, 'id'>,
): Promise<Student> => {
    const { email } = newStudentData;

    const foundStudent = await studentsRepository.findOneBy({ email });

    if (foundStudent)
        throw new HttpException(
            HttpStatusesEnum.CONFLICT,
            HttpErrorMessagesEnum.STUDENT_ALREDY_EXIST,
        );

    const newStudent = studentsRepository.save(
        newStudentData,
    ) as Promise<Student>;

    return newStudent;
};

export const updateStudentById = async ({
    studentId,
    studentData,
}: IStudentUpdateServiceParams): Promise<Student> => {
    const foundGroup = await groupsRepository.findOne({
        where: { id: studentData.groupId },
    });

    if (studentData.groupId && !foundGroup)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.GROUP_NOT_FOUND,
        );

    const updateStudent = await studentsRepository.update(
        studentId,
        studentData,
    );

    if (!updateStudent.affected)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    const result = studentsRepository.findOneBy({
        id: studentId,
    }) as Promise<Student>;

    return result;
};

export const getStudentImageById = async (
    studentId: string,
): Promise<string> => {
    const foundStudent = await studentsRepository.findOneBy({
        id: studentId,
    });

    if (!foundStudent)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    if (!foundStudent.imagePath)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            'File is not provided',
        );

    return foundStudent.imagePath;
};

export const addToStudentImageById = async ({
    studentId,
    filePath,
}: IStudentAddImageServiceParams): Promise<Student> => {
    const studentToUpdate = await studentsRepository.findOneBy({
        id: studentId,
    });

    if (!studentToUpdate)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    // Checking if path for image exist
    if (!filePath)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            'Student file is not provided',
        );

    try {
        // Create unique id for image
        const imageId = uuidv4();

        // Get image format (.png, .jpg and else)
        const imageExtention = path.extname(filePath);

        // Create new image name
        const imageName = imageId + imageExtention;

        // Students directory name in public folder
        const studentsDirectoryName = 'students';

        // Get full path to folder
        const studentsDirectoryPath = path.join(
            __dirname,
            '../',
            'public',
            studentsDirectoryName,
        );

        // The path where we move the picture
        const newImagePath = path.join(studentsDirectoryPath, imageName);

        // Path for field imagePath in IStudent
        const imagePath = `${studentsDirectoryName}/${imageName}`;

        // Resize image to a maximum width of 250 pixels and
        await sharp(filePath).resize(250, null).toFile(newImagePath);

        await studentsRepository.update(studentId, {
            imagePath,
        });

        const result = studentsRepository.findOneBy({
            id: studentId,
        }) as Promise<Student>;

        return result;
    } catch (error) {
        // If error we delete file and throw error
        await fs.unlink(filePath);
        throw error;
    }
};

export const addToStudentGroupById = async ({
    studentId,
    groupId,
}: IStudentAddGroupServiceParams): Promise<Student> => {
    const studentToUpdate = await studentsRepository.findOneBy({
        id: studentId,
    });

    if (!studentToUpdate)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    const foundGroup = await groupsRepository.findOneBy({
        id: groupId,
    });

    if (!foundGroup)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.GROUP_NOT_FOUND,
        );

    await studentsRepository.update(studentId, { groupId });

    const result = studentsRepository.findOneBy({
        id: studentId,
    }) as Promise<Student>;

    return result;
};

export const deleteStudentById = async (
    studentId: string,
): Promise<DeleteResult> => {
    const result = await studentsRepository.delete(studentId);

    if (!result.affected)
        throw new HttpException(
            HttpStatusesEnum.NOT_FOUND,
            HttpErrorMessagesEnum.STUDENT_NOT_FOUND,
        );

    return result;
};
