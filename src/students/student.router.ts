import { Router } from 'express';

import uploadMiddleware from '../application/middlewares/upload.middleware';

import controllerWrapper from '../application/utilities/controller-wrapper.utility';

import validator from '../application/middlewares/validation.middleware';

import {
    generateQuerySchema,
    idParamSchema,
} from '../application/schemas/id-param.schema';

import * as studentController from './student.controller';

import {
    studentAddGroupSchema,
    studentCreateSchema,
    studentUpdateSchema,
} from './student.schema';

const router = Router();

router.get(
    '/',
    validator.query(generateQuerySchema('name')),
    controllerWrapper(studentController.getAllStudents),
);

router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentController.getStudentById),
);

router.post(
    '/',
    validator.body(studentCreateSchema),
    controllerWrapper(studentController.createStudent),
);

router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(studentUpdateSchema),
    controllerWrapper(studentController.updateStudentById),
);

router.get(
    '/:id/image',
    validator.params(idParamSchema),
    controllerWrapper(studentController.getStudentImageById),
);

router.patch(
    '/:id/image',
    validator.params(idParamSchema),
    uploadMiddleware.single('file'),
    controllerWrapper(studentController.addToStudentImageById),
);

router.patch(
    '/:id/group',
    validator.params(idParamSchema),
    validator.body(studentAddGroupSchema),
    controllerWrapper(studentController.addToStudentGroupById),
);

router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentController.deleteStudentById),
);

export default router;
