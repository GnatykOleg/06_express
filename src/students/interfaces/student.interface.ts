import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

import { Student } from '../entities/student.entity';

export interface IStudentCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<Student, 'id'>;
}

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<Student>;
    [ContainerTypes.Params]: { id: string };
}

export interface IStudentAddImageRequest extends ValidatedRequestSchema {
    [ContainerTypes.Params]: { id: string };
    file: Express.Multer.File;
}

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: { groupId: string };
    [ContainerTypes.Params]: { id: string };
}

export interface IGetStudentByName extends ValidatedRequestSchema {
    [ContainerTypes.Query]: { name: string };
}

interface IStudentId {
    studentId: string;
}

export interface IStudentWithGroupName extends Omit<Student, 'groupId'> {
    groupName: string;
}

export interface IStudentUpdateServiceParams extends IStudentId {
    studentData: Partial<Student>;
}

export interface IStudentAddGroupServiceParams extends IStudentId {
    groupId: string;
}

export interface IStudentAddImageServiceParams extends IStudentId {
    filePath?: string;
}
