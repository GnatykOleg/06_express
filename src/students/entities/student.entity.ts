import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
} from 'typeorm';

import CoreEntity from '../../application/entities/core.entity';

import { Group } from '../../groups/entities/group.entity';

import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
    @Column({ type: 'varchar', nullable: false, length: 50 })
    @Index()
    name: string;

    @Column({ type: 'varchar', nullable: false, length: 50 })
    surname: string;

    @Column({ type: 'varchar', nullable: false, length: 50 })
    email: string;

    @Column({ type: 'integer', nullable: false })
    age: number;

    @Column({ name: 'image_path', nullable: true, length: 250 })
    imagePath: string;

    @ManyToOne(() => Group, (group) => group.students, {
        nullable: true,
        eager: false,
    })
    @JoinColumn({ name: 'group_id' })
    group: Group;

    @Column({ type: 'uuid', nullable: true, name: 'group_id' })
    groupId: string;

    @OneToMany(() => Mark, (mark) => mark.student)
    marks: Mark[];
}
