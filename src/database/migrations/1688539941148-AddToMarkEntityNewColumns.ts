import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddToMarkEntityNewColumns1688539941148
    implements MigrationInterface
{
    name = 'AddToMarkEntityNewColumns1688539941148';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "course_id" SET NOT NULL`,
        );
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "student_id" SET NOT NULL`,
        );
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "lector_id" SET NOT NULL`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "lector_id" DROP NOT NULL`,
        );
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "student_id" DROP NOT NULL`,
        );
        await queryRunner.query(
            `ALTER TABLE "marks" ALTER COLUMN "course_id" DROP NOT NULL`,
        );
    }
}
