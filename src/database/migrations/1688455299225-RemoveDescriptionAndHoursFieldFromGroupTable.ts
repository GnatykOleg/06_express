import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveDescriptionAndHoursFieldFromGroupTable1688455299225
    implements MigrationInterface
{
    name = 'RemoveDescriptionAndHoursFieldFromGroupTable1688455299225';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "groups" DROP COLUMN "description"`,
        );
        await queryRunner.query(`ALTER TABLE "groups" DROP COLUMN "hours"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "groups" ADD "hours" integer NOT NULL`,
        );
        await queryRunner.query(
            `ALTER TABLE "groups" ADD "description" text NOT NULL`,
        );
    }
}
