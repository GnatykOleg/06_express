import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateStudentTable1688376522116 implements MigrationInterface {
    name = 'CreateStudentTable1688376522116';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "students" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying(50) NOT NULL, "surname" character varying(50) NOT NULL, "email" character varying(50) NOT NULL, "age" integer NOT NULL, "image_path" character varying(250), CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "students"`);
    }
}
