export enum HttpStatusesEnum {
    OK = 200,
    CREATED = 201,
    NO_CONTENT = 204,
    BAD_REQUEST = 400,
    NOT_FOUND = 404,
    CONFLICT = 409,
    ISE = 500,
}

export enum HttpErrorMessagesEnum {
    STUDENT_NOT_FOUND = 'We did not find such a student',
    STUDENT_ALREDY_EXIST = 'Such a student already exists',

    GROUP_NOT_FOUND = 'We did not find such a group',
    GROUP_ALREDY_EXIST = 'Such a group already exists',

    LECTOR_NOT_FOUND = 'We did not find such a lector',
    LECTOR_ALREDY_EXIST = 'Such a lector already exists',

    COURSE_NOT_FOUND = 'We did not find such a course',
    COURSE_ALREDY_EXIST = 'Such a course already exists',

    MARK_NOT_FOUND = 'We did not find such a mark',
    MARK_ALREDY_EXIST = 'Such a mark already exists',
}
