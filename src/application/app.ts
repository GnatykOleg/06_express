import express from 'express';

import studentsRouter from '../students/student.router';

import groupsRouter from '../groups/group.router';

import lectorsRouter from '../lectors/lector.router';

import coursesRouter from '../courses/course.route';

import marksRouter from '../marks/mark.router';

import cors from 'cors';

import logger from './middlewares/logger.middleware';

import bodyParser from 'body-parser';

import path from 'path';

import exceptionsFilter from './middlewares/exceptions-filter.middleware';

import AppDataSource from '../configurations/database/data-source.configuration';

// Init express
const app = express();

// Cors policy
app.use(cors());

// Logging routes
app.use(logger);

// JSON body parser
app.use(bodyParser.json());

// Connect to DB
AppDataSource.initialize()
    .then(() => console.log('TypeORM connected to database!'))
    .catch((error) => console.error('Error TypeORM connected:', error));

// Routing:

// Get static files
const staticFilesPath = path.join(__dirname, '../', 'public');
app.use('/api/v1/public', express.static(staticFilesPath));

app.use('/api/v1/students', studentsRouter);

app.use('/api/v1/groups', groupsRouter);

app.use('/api/v1/lectors', lectorsRouter);

app.use('/api/v1/courses', coursesRouter);

app.use('/api/v1/marks', marksRouter);

// Exceptions filter for errors handling, should be the last
app.use(exceptionsFilter);

export default app;
