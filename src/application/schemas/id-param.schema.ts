import Joi from 'joi';

import { IDynamicKey } from '../interfaces/application.interface';

export const idParamSchema = Joi.object<{ id: string }>({
    id: Joi.string().lowercase().guid().required(),
});

export const generateQuerySchema = (
    query: string,
): Joi.ObjectSchema<IDynamicKey> =>
    Joi.object<IDynamicKey>({
        [query]: Joi.string().lowercase().optional(),
    });
