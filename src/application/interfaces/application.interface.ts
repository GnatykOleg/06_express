export interface IDynamicKey {
    [key: string]: string;
}
