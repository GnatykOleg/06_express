import { Request, Response, NextFunction } from 'express';

const logger = (request: Request, _: Response, next: NextFunction): void => {
    const { url, method } = request;

    console.log(`URL: ${url} | Method: ${method}`);

    next();
};

export default logger;
