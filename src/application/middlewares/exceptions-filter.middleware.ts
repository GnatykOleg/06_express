import { Request, Response, NextFunction } from 'express';

import HttpException from '../exceptions/http.exception';

import { HttpStatusesEnum } from '../enums/application.enum';

const exceptionsFilter = (
    error: HttpException,
    _: Request,
    response: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    next: NextFunction,
): void => {
    const status = error.status || HttpStatusesEnum.ISE;
    const message = error.message || 'Internal server error';
    response.status(status).json({ message, status });
};

export default exceptionsFilter;
