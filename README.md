# 06_express with typeorm

06_express is a CRUD server that is used with the Express framework, which provides an API to work with the "Student" and "Group" requirements.

The Student resource contains student information such as first name, last name, email, age, image path, and group ID. The server provides the ability to get a list of all students, get information about a student by his ID, including a new student, select student data, delete a student, pick up and get an image (avatar) of a student, and add a student to a group.

The "Group" resource contains information about the population, represented by an identifier and a name. The server allows you to get a list of all groups, get information about a group by its ID, include a new group, include group data, and delete groups.

All data processing operations using TypeScript, a database represented as a JavaScript array, object identifiers in the ObjectId format, a server for the correct processing of streams, validation of parameters and requests of the body, and also returns the status of periodic responses reflecting the results of operations or errors.

## Installation

#### To use this project, follow the instructions below:

-   Clone the repository to your local machine. `git clone [repository_url]`

-   Change to the project directory. `cd 06_express`

-   Install the required dependencies with `npm install`.

## Usage

### Running the linter and formatting the code

-   You can use a possible command to check code style and formatting: `npm run lint`

-   Fix code style bug: `npm run lint:fix`

-   Checking the code with the established formatting protocol: `npm run format:check`

-   Automatically format code according to the rules: `npm run format:write`

### Building the project

-   To build the project, run the command: `npm run build` This command will lint the code and check the format before compiling TypeScript.

### Run application

-   Run in production mode: `npm run start`

-   Run in development algorithm with reload on file changes: `npm run start:dev` Includes nodemon&ts-node to work with TypesScript

### Run TypeORM with migrations

-   Run TypeORM CLI with TypeScript support and load environment variables from .env file `npm run typeorm`

-   Configuring TypeORM using the specified database configuration file. `npm run typeform:config`

-   Create a new migration with the specified name `npm run migrations:create`

-   Generate new migration based on current database state `npm run migrations:generate`

-   Revert last applied migration `npm run migrations:revert`

-   Apply all available migrations in database `npm run migrations:run`
